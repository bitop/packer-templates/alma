/*
    DESCRIPTION:
    Build account variables used for all builds.
    - Variables are passed to and used by guest operating system configuration files (e.g., ks.cfg, autounattend.xml).
    - Variables are passed to and used by configuration scripts.
*/

// Default Account Credentials
build_username           = "builder"
build_password           = "builder"
build_password_encrypted = "$5$w/Ockjew3KzSrHk4$py/fP855mgKzVf2GNQBL1GbklAvjqgY6jXjuwskbNaC"